<?php 
/*
Plugin Name:    TU Advanced Media Box
Version:        0.0.1
Description:    A plugin for media selecting in Wordpress. Support images/video, docs file, pdf file...
Author:         Vũ Ngọc Tùng | Ngoc Tung Vu (Mr)
Author URI:     http://tungvn.info/
Plugin URI:     https://gitlab.com/tungvn/tu-advanced-media-box
Text Domain:    tu-advanced-media-box
License:        GPLv2

Copyright 2016  Vũ Ngọc Tùng
*/

/* Public functions */
// Add necessary libaries
add_action( 'admin_enqueue_scripts', 'tvn_admin_enqueue_scripts_styles' );

// Help functions
add_action( 'wp_ajax_backend__tvn_get_image_url_by_id', 'backend__tvn_get_image_url_by_id_cb' );
add_action( 'wp_ajax_backend__tvn_get_image_data_by_id', 'backend__tvn_get_image_data_by_id_cb' );
add_action( 'wp_ajax_backend__tvn_save_image_data', 'backend__tvn_save_image_data_cb' );

// Add necessary libaries
function tvn_admin_enqueue_scripts_styles() {
	// CSS
	wp_enqueue_style( 'tvn-advanced-media-box-css', get_template_directory() . '/includes/tu-advanced-media-box/asset/admin.css', array(), '1.0' );

	// Scripts
	wp_enqueue_media();
	wp_enqueue_script( 'jquery-ui-core' );
	wp_enqueue_script( 'jquery-ui-resizable' );
	wp_enqueue_script( 'jquery-ui-sortable' );
	wp_enqueue_script( 'tvn-advanced-media-box-js', get_template_directory() . '/includes/tu-advanced-media-box/asset/admin.js', array( 'jquery-ui-core' ), '1.0' );
}

/**
 * @function tvn_advanced_media_box_callback
 * @description Display box and initialize scripts
 * 
 * @param $name: <string> Name of box. (required)
 * @param $key: <string> Key of return array. Note: use key for multi box together one page. Default is 'tvn_media' (required)
 * @param $type: <string|array> Type of media which is selected. String with comma seperate each type ('image', 'application/pdf', 'video/mp4',...). Default 'image' (option)
 * @param $attachments <array> Array of saved media id. Box will show these media. Default empty array (option)
 * @param $multiple: <boolean> Select mutiple or not. Default true (option)
 */
function tvn_advanced_media_box_callback( $name, $key = 'tvn_media', $type = 'image', $attachments = array(), $multiple = true ) {
	$id_of_box = 
	$type_str = '';
	if( is_array( $type ) ) {
		$type = implode( ',', $type );
	}

	$type_str = '[' . $type . ']'; ?>

	<div class="tvn-media-box">
		<div class="tvn-gallery-label">
			<label for=""><?php echo $name; ?></label>
			<p class="desc"><small>Ext: jpg, png, jpeg</small></p>
		</div>
		
		<div class="tvn-gallery-field">
			<div class="tvn-gallery-main">
				<ul class="tvn-gallery-input">
				<?php foreach($attachments as $bnkey => $bn): ?>
					<li class="tvn-gallery-item">
						<input type="hidden" class="save-image" name="<?php echo $key; ?>[]" value="<?php echo intval( $bn ); ?>">
						<div class="item">
							<img src="<?php echo wp_get_attachment_url( $bn ); ?>" alt="">
						</div>
						<a class="cta tvn-gallery-remove-item" href="#remove" title="Remove">x</a>
					</li>
				<?php endforeach; ?>
				</ul>
				
				<div class="cta-gallery-toolbar" style="background-color: #fff;">
					<a class="cta button button-primary tvn-gallery-add-item" 
					   href="#add-media" 
					   data-library="<?php echo $type_str; ?>" 
					   data-multiple="<?php echo $multiple; ?>">
						Add Media
					</a>
				</div>
			</div>

			<div class="tvn-gallery-side">
				<div class="tvn-gallery-side-inner">
					<div class="tvn-gallery-side-data"></div>
					<div class="cta-gallery-toolbar" style="background-color: #f1f1f1;">
						<a class="cta button button-primary tvn-gallery-side-submit" href="#submit-side">Update</a>
						<span class="spinner"></span>
						<a class="cta button tvn-gallery-side-close" href="#close-side">Close</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
    jQuery(document).ready(function($) {
        var file_frame;

        // Add images
        $('.tvn-gallery-add-item').click(function(event) {
            event.preventDefault();

            var _this = $(this);
            var _multi = Boolean( parseInt( $(this).data('multiple') ) );
            var container = _this.parents('.tvn-gallery-main').children('.tvn-gallery-input');

            if( file_frame ) {
                file_frame.open();
                return;
            }

            file_frame = wp.media.frames.file_frame = wp.media({
                title: 'Add media',
                library: {type: _this.data('library')},
                button: {text: 'Select'}, 
                multiple: _multi
            });

            var images_id = _this.parents('.tvn-media-box').find('.save-image').map( function() { return $(this).val(); } );

            file_frame.on('open', function() {
                var selection = file_frame.state().get('selection');
                $.each( images_id, function(index, el) {
                    var attachment = wp.media.attachment( el );
                    attachment.fetch();
                    selection.add( attachment ? [ attachment ] : [] );
                });
            });

            file_frame.on('select', function() {
                attachment = file_frame.state().get('selection');
                if( !_multi ){
                    attachment = attachment.first().toJSON();
                    if(typeof attachment.id != 'undefined'){
                        $.ajax({
                            url: '<?php echo admin_url("admin-ajax.php"); ?>',
                            type: 'POST',
                            dataType: 'html',
                            data: {action: 'backend__tvn_get_image_url_by_id', id: attachment.id}
                        })
                        .done(function( image_url ) {
                            html += '<li class="tvn-gallery-item">';
                            html += '   <input type="hidden" class="save-image" name="tvn_gallery[]" value="'+ attachment.id +'">';
                            html += '   <div class="item">';
                            html += '       <img src="'+ image_url +'" alt="">';
                            html += '   </div>';
                            html += '   <a class="cta tvn-gallery-remove-item" href="#remove" title="Remove">x</a>';
                            html += '</li>';

                            container.append( html );
                            container.sortable();
                        });
                    }
                }
                else{
                    attachment = attachment.toJSON();
                    container.html( '' );
                    $.each(attachment, function(index, el) {
                        var html = '';
                        if(typeof el.id != 'undefined'){
                            $.ajax({
                                url: '<?php echo admin_url("admin-ajax.php"); ?>',
                                type: 'POST',
                                dataType: 'html',
                                data: {action: 'backend__tvn_get_image_url_by_id', id: el.id}
                            })
                            .done(function( image_url ) {
                                html += '<li class="tvn-gallery-item">';
                                html += '   <input type="hidden" class="save-image" name="tvn_gallery[]" value="'+ el.id +'">';
                                html += '   <div class="item">';
                                html += '       <img src="'+ image_url +'" alt="">';
                                html += '   </div>';
                                html += '   <a class="cta tvn-gallery-remove-item" href="#remove" title="Remove">x</a>';
                                html += '</li>';

                                container.append( html );
                                container.sortable();
                            });
                        }
                    });
                }
            });

            file_frame.open();
        });

        // Edit image and attributes
        $('.tvn-gallery-main').delegate('img', 'click', function(event) {
            var field = $(this).parents('.tvn-gallery-field');
            var main = $(this).parents('.tvn-gallery-main');
            var side = $(field).children('.tvn-gallery-side');
            var side_data = $(field).find('.tvn-gallery-side-data');
            var img_id = $(this).parents('li').children('input.save-image').val();

            $.ajax({
                url: '<?php echo admin_url("admin-ajax.php"); ?>',
                type: 'POST',
                dataType: 'html',
                data: {action: 'backend__tvn_get_image_data_by_id', id: img_id}
            })
            .done(function( html ) {
                side_data.html( html );

                main.animate({'right': '310px'}, 300);
                side.animate({'right': '0'}, 300);
            });

        });

        // Remove images
        $('.tvn-gallery-main').delegate('.tvn-gallery-remove-item', 'click', function(event) {
            event.preventDefault();
            if( confirm( 'Can not rollback! Are you sure?' ) )
                $(this).parents('li').remove();

            return false;
        });

        // Submit side
        $('.tvn-gallery-side-submit').click(function(event) {
            event.preventDefault();
            var spinner = $(this).siblings('.spinner');
            $(spinner).addClass('is-active');

            $.ajax({
                url: '<?php echo admin_url("admin-ajax.php"); ?>',
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'backend__tvn_save_image_data',
                    id: $('.tvn-side-id').val(),
                    title: $('.tvn-side-title').val(),
                    url: $('.tvn-side-url').val(),
                    desc: $('.tvn-side-desc').val()
                }
            })
            .done(function( response ) {
                $(spinner).removeClass('is-active');
            });
        });

        // Close side_data
        $('.tvn-gallery-side-close').click(function(event) {
            event.preventDefault();
            var field = $(this).parents('.tvn-gallery-field');
            var main = $(field).children('.tvn-gallery-main');
            var side = $(field).children('.tvn-gallery-side');

            main.animate({'right': '0'}, 300);
            side.animate({'right': '-310px'}, 300);
        });
    });
    </script>
<?php }

// Get 
function backend__tvn_get_image_url_by_id_cb() {
	if( !isset( $_POST['id'] ) ) {
		echo '';
		exit;
	}

	$id = intval( $_POST['id'] );

	if( 'attachment' == get_post_type( $id ) )
		echo wp_get_image_src( $id, 'large' );

	exit;
}


function backend__tvn_get_image_data_by_id_cb() {
	if( !isset( $_POST['id'] ) ) {
		echo '';
		exit;
	}

	$id = intval( $_POST['id'] );
	$html = '';

	if( 'attachment' == get_post_type( $id ) ) {
		$data = get_post( $id );
		$metadata = wp_get_attachment_metadata( $id );

		$title = get_the_title( $id );
		$desc = $data->post_content;
		// $slug = $data->post_name;
		$caption = $data->post_excerpt;
		$filename = basename( $data->guid );
		$date = get_the_time( 'd/m/Y', $id );
		$size = $metadata['width'] .'x'. $metadata['height'];

		$html .= '<div class="tvn-gallery-info">';
		$html .= '<input type="hidden" class="tvn-side-id" value="'. $id .'">';
		$html .= '<img src="'. wp_get_image_src( $id, 'thumbnail' ) .'" alt="">';
		$html .= '<p class="info-name">'. $filename .'</p>';
		$html .= '<p class="info-date">'. $date .'</p>';
		$html .= '<p class="info-dimension">'. $size .'</p>';
		$html .= '<div class="clear"></div>';
		$html .= '</div>';
		$html .= '<table class="form-table">';
		$html .= '<tbody>';
		$html .= '<tr>';
		$html .= '<td>Title</td>';
		$html .= '<td><input type="text" class="tvn-side-title" name="tvn_attachment['. $id .'][title]" id="" value="'. $title .'"></td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>URL</td>';
		$html .= '<td><input type="text" class="tvn-side-url" name="tvn_attachment['. $id .'][url]" id="" value="'. $caption .'"></td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>Description</td>';
		$html .= '<td><textarea class="tvn-side-desc" name="tvn_attachment['. $id .'][desc]" id="">'. $desc .'</textarea></td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= '<td>Alt Text</td>';
		$html .= '<td><input type="text" name="tvn_attachment['. $id .'][alt]" id="" value="'. $slug .'"></td>';
		$html .= '</tr>';
		$html .= '</tbody>';
		$html .= '</table>';

		echo $html;
	}
	
	exit;
}


function backend__tvn_save_image_data_cb() {
	if( !isset( $_POST['id'] ) ) {
		echo '';
		exit;
	}

	$id = intval( $_POST['id'] );
	$title = sanitize_text_field( $_POST['title'] );
	$url = sanitize_text_field( $_POST['url'] );
	$desc = sanitize_text_field( $_POST['desc'] );

	wp_update_post( array(
		'ID' => $id,
		'post_title' => $title,
		'post_excerpt' => $url,
		'post_content' => $desc,
	) );

	echo json_encode( array( 'status' => 1 ) );
	exit;
}
