jQuery(document).ready(function($) {
    // Resizable init
    if( $('body').find('.tvn-gallery-field').length > 0 ) {
	    $('.tvn-gallery-field').resizable({
	        handles: 's',
	        minHeight: 500
	    });
    }
    
    // Sortable init
    if( $('body').find('.tvn-gallery-input').length > 0 ) {
    	$('.tvn-gallery-input').sortable();
    }
});
